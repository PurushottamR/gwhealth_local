﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GWHealth.Filters
{
    public class AddHeaderAttribute : ResultFilterAttribute
    {
        private readonly string _name;
        private readonly string _value;

        public AddHeaderAttribute(string name, string value)
        {
            _name = name;
            _value = value;
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            context.HttpContext.Response.Headers.Add(_name, new string[] { _value });
            base.OnResultExecuting(context);
        }
    }


    public class SecureRequest : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {

            if (context.HttpContext.Request.Cookies.Count > 0)
            {
                if (context.HttpContext.Request.Cookies["Auth"] != null)
                {
                    if (context.HttpContext.User.Claims.ToList().Count > 0)
                    {
                        var userId = context.HttpContext.User.Claims.FirstOrDefault().Value;
                        var DisplayName = context.HttpContext.User.Claims.ToList()[1].Value;

                        context.ActionArguments.Add("userId", userId); 

                        return;

                    }
                }
            }           

            context.Result = new BadRequestObjectResult("Not Secure");
        }
                    
                    }


    


}
